/** @format */

const webpack = require('webpack')
const config = require('../../webpack.server')
const compiler = webpack(config)

compiler.run((err, stats) => {
  if (err) console.log('error')
  else console.log(stats)
})
