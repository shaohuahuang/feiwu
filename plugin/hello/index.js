class MyPlugin {
  constructor(options) {
    this.options = options;
  }
  apply(compiler){
    compiler.hooks.done.tap('MyPlugin', (stats) => {
      console.log('MyPlugin ', this.options);
    });

    compiler.hooks.done.tapAsync('DonePlugin', (stats, callback) => {
      console.log('Hello ', this.options.name);
      setTimeout(() => {console.log(1);}, 1000);
      setTimeout(() => {console.log(2);}, 2000);
      setTimeout(() => {console.log(3);}, 3000);
      setTimeout(() => {
        callback();
      }, 4000)
    })

    compiler.hooks.emit.tap('xxx', compilation => {
      let assets = compilation.assets;
      console.log(assets);
    })
  }
}

module.exports = MyPlugin;