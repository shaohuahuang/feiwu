/** @format */

import * as React from 'react'
import {BrowserRouter as Router, Route} from 'react-router-dom'
import Home from './components/Home'
import About from './components/About'

//console.log('===========================test')
console.log('===========================test')
const App = (): any => {
  return (
    <Router>
      <Route exact path="/about" component={About}></Route>
      <Route exact path="/" component={Home}></Route>
    </Router>
  )
}

export default App
