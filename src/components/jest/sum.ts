/** @format */

const sum = (a: number, b: number): any => {
  return a + b
}

export default sum
