var path = require("path");
var webpack = require("webpack");

module.exports = {
    entry: {
        vendor: ['react','react-dom','react-router-dom','react-refresh/runtime']
    },
    mode: 'development',
    output: {
        path: path.join(__dirname, "dist", "dll"),
        filename: "dll.[name].js",
        library: "[name]"
    },
    resolve: {
      extensions: [".js", ".jsx"]
    },
    plugins: [
        new webpack.DllPlugin({
            path: path.join(__dirname, "dist/dll", "[name]-manifest.json"),
            name: "[name]"
        })
    ]
};
