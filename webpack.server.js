var webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const path = require('path');

module.exports = {
  entry: [
    "./src/entry.tsx",
  ],
  output: {
    path: path.resolve(__dirname,'dist'),
    filename: "bundle.js"
  },
  mode: 'development',
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    hot: true
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              plugins: [require.resolve('react-refresh/babel')]
            }
          },
          {
            loader: 'ts-loader',
            options: {
              happyPackMode: true,
              transpileOnly: true
            }
          }
        ]
      },
      {
        test: /\.scss?$/,
        exclude: /node_modules/, 
        use: ['style-loader','css-loader','sass-loader']
      }
    ]
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
  },
  plugins: [
    new ReactRefreshWebpackPlugin(),
    new HtmlWebpackPlugin({
      title: 'My App',
      template: 'assets/index.html'
    }),
    new webpack.DllReferencePlugin({
      context: __dirname,
      manifest: require('./dist/dll/vendor-manifest.json'),
    })
  ]
}